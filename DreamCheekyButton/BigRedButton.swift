//
//  BigRedButton.swift
//  KuandoSwift
//
//  Created by Eric Betts on 6/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import IOKit.hid

protocol DeviceDelegate {
    func deviceConnected(portal : BigRedButton)
    func deviceDisconnected(portal : BigRedButton)
    func input(report: NSData)
}

class BigRedButton : NSObject {
    let vendorId = 0x1D34
    let productId = 0x000D
    let reportSize = 16
    static let singleton = BigRedButton()
    var device : IOHIDDevice? = nil
    var delegate : DeviceDelegate? = nil
    var buttonThread : NSThread?
    
    func input(inResult: IOReturn, inSender: UnsafeMutablePointer<Void>, type: IOHIDReportType, reportId: UInt32, report: UnsafeMutablePointer<UInt8>, reportLength: CFIndex) {
        let report = NSData(bytes: report, length: reportLength)
        //print("IN: \(report)")
        self.delegate?.input(report)
    }
    
    func output(data: NSData) {
        if (data.length > reportSize) {
            print("output data too large for USB report")
            return
        }
        let reportId : CFIndex = 0
        if let button = device {
            //print("OUT: \(data)")
            IOHIDDeviceSetReport(button, kIOHIDReportTypeOutput, reportId, UnsafePointer<UInt8>(data.bytes), data.length);
        }
    }
    
    func connected(inResult: IOReturn, inSender: UnsafeMutablePointer<Void>, inIOHIDDeviceRef: IOHIDDevice!) {
        print("Device connected")
        // It would be better to look up the report size and create a chunk of memory of that size
        let report = UnsafeMutablePointer<UInt8>.alloc(reportSize)
        device = inIOHIDDeviceRef
        
        let inputCallback : IOHIDReportCallback = { inContext, inResult, inSender, type, reportId, report, reportLength in
            let this : BigRedButton = unsafeBitCast(inContext, BigRedButton.self)
            this.input(inResult, inSender: inSender, type: type, reportId: reportId, report: report, reportLength: reportLength)
        }
        
        //Hook up inputcallback
        IOHIDDeviceRegisterInputReportCallback(device, report, reportSize, inputCallback, unsafeBitCast(self, UnsafeMutablePointer<Void>.self));
        
        self.delegate?.deviceConnected(self)
    }
    
    func removed(inResult: IOReturn, inSender: UnsafeMutablePointer<Void>, inIOHIDDeviceRef: IOHIDDevice!) {
        print("Device removed")
        self.delegate?.deviceDisconnected(self)
    }
    
    func discover() {
        self.buttonThread = NSThread(target: self, selector:#selector(start), object: nil)
        
        if let thread = buttonThread {
            thread.start()
        }
    }
    
    func start() {
        let deviceMatch = [kIOHIDProductIDKey: productId, kIOHIDVendorIDKey: vendorId ]
        let managerRef = IOHIDManagerCreate(kCFAllocatorDefault, IOOptionBits(kIOHIDOptionsTypeNone)).takeUnretainedValue()
        
        IOHIDManagerSetDeviceMatching(managerRef, deviceMatch)
        IOHIDManagerScheduleWithRunLoop(managerRef, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
        IOHIDManagerOpen(managerRef, 0);
        
        let matchingCallback : IOHIDDeviceCallback = { inContext, inResult, inSender, inIOHIDDeviceRef in
            let this : BigRedButton = unsafeBitCast(inContext, BigRedButton.self)
            this.connected(inResult, inSender: inSender, inIOHIDDeviceRef: inIOHIDDeviceRef)
        }
        
        let removalCallback : IOHIDDeviceCallback = { inContext, inResult, inSender, inIOHIDDeviceRef in
            let this : BigRedButton = unsafeBitCast(inContext, BigRedButton.self)
            this.removed(inResult, inSender: inSender, inIOHIDDeviceRef: inIOHIDDeviceRef)
        }
        
        IOHIDManagerRegisterDeviceMatchingCallback(managerRef, matchingCallback, unsafeBitCast(self, UnsafeMutablePointer<Void>.self))
        IOHIDManagerRegisterDeviceRemovalCallback(managerRef, removalCallback, unsafeBitCast(self, UnsafeMutablePointer<Void>.self))
        
        NSRunLoop.currentRunLoop().run();
    }

}