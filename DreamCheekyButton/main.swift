//
//  main.swift
//  DreamCheekyButton
//
//  Created by Eric Betts on 6/23/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
import AppKit

if (Process.arguments.count < 4) {
    print("Usage: \(Process.arguments[0]) <open> <close> <press>")
    exit(1)
}


class Action {
    let commands : [String:NSURL]
    
    
    init(open: String, close: String, press: String) {
        commands = [
            "open": NSURL(string: open)!,
            "close": NSURL(string: close)!,
            "press": NSURL(string: press)!,
            ]
    }
    
    func open() {
        run("open")
    }
    
    func close() {
        run("close")
    }
    
    func press() {
        run("press")
    }
    
    func run(key: String) {
        if let command = commands[key] {
            if (command.absoluteString != "") {
                NSWorkspace.sharedWorkspace().openURL(command)
                //Swift3: UIApplication.shared().openURL(URL(string: "...")!)
            }
        }
    }
}


class Main : DeviceDelegate {
    enum State : UInt8 {
        case closed    = 0x15
        case press     = 0x16
        case open      = 0x17
    }
    
    let interval = 0.3
    let action : Action
    let button : BigRedButton = BigRedButton.singleton
    var currentState : State = .closed //Assume it starts closed
    
    init(action: Action) {
        self.action = action
        self.button.delegate = self
        self.button.discover()
    }
    
    func input(report: NSData) {
        if let newState = State(rawValue: report[0]) {            
            print("\(currentState) => \(newState)")
            //We limit the transitions handled to prevent things like press -> open causing an open event
            if (currentState == .closed && newState == .open) {
                currentState = .open
                action.open()
            } else if (currentState == .open && newState == .press) {
                currentState = .press
                action.press()
            } else if (currentState == .open && newState == .closed) {
                currentState = .closed
                action.close()
            } else if (currentState == .closed && newState == .press) {
                //Special case because we missed the 'open' transition
                currentState = .press
                action.open()
                action.press()
            }
            self.currentState = newState
        }
    }
    
    @objc func statusUpdate() {
        let statusRequest = NSData(bytes: [0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02] as [UInt8], length: 8)
        self.button.output(statusRequest)
    }
    
    func deviceConnected(button : BigRedButton) {
        NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: #selector(Main.statusUpdate), userInfo: nil, repeats: true)
    }
    func deviceDisconnected(button :  BigRedButton) {}
}

let open = Process.arguments[1]
let close = Process.arguments[2]
let press = Process.arguments[3]
print("\(open) | \(close) | \(press)")

let action = Action(open: open, close: close, press: press)
let main = Main(action: action)

NSRunLoop.currentRunLoop().run()